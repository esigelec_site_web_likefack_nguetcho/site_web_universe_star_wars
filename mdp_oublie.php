<?php 
    session_start();
    include 'database.php';

    if(isset($_SESSION['id']))
    {
        header("location: Connexion.php");
        exit;
    }

    if(!empty($_POST))
    {
        extract($_POST);
        
        if(empty($_POST['email']))
        {
            $err_mail = "Veuillez entrer votre adresse mail";
        }

        else
        {
            $req_verification = "SELECT * FROM user WHERE email = ?";
            $verification = $DB->query($req_verification, array($_POST['email']));
            $verification = $verification->fetch();

            if(isset($verification['mail']))
            {
                $from_mail = "ne-pas-repondre@esigelec.com";
                $to = $verification['mail'];
                $subject = "nouveau mot de passe";
                $body = "Bonjour " .$verification['name']. "<p>Veuillez <a href='nouveau_mdp.php'>cliquer ici</a> pour entrer un nouveau mot de passe</p>";

                require 'send_mail.php';

                $mail_sent = new send_mail($from_mail);
                $mail_sent->send($to, $subject, $body);

            ?> 
            
        <?php } ?>

        <?php } ?>

    <?php } ?>
 