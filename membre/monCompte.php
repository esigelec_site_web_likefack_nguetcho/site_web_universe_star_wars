<?php
	session_start();
	include('../database.php');

	//on vérifie s'il y a une session en cours
	if (isset($_SESSION['id'])) {
		header("Location: ../Connexion.php");
		exit;
	}

	$req_affichage = "SELECT * FROM user WHERE id = ?";
	$req=$DB->query($req_affichage, array($SESSION['id']));

	$req=$req->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mes Informations</title>
</head>
<body>

	<h2>Informations sur mon profil</h2>
	<div>
		
		<ul>
			<li>id : <?= $req['id']?></li>
			<li>name : <?= $req['name']?></li>
			<li>Pseudo : <?= $req['pseudo']?></li>
			<li>Adresse électronique : <?= $req['email']?></li>
		</ul>
	</div>
</body>
</html>