<?php 
     class Vote
     {
        private $DB;

         public function __construct(connexionDB $DB)
         {
             $this->DB = $DB;
         }

         public function like($ref, $id_Film, $user_id)
         {
            $req = "SELECT * FROM $ref WHERE id = ?";
            $request = $this->DB->query($req, array($id_Film));

            if($request->rowCount() > 0)
            {
                $req_verify = "SELECT * FROM vote WHERE id_Film = ? AND id_Membre = ?";
                $result_verify = $this->DB->query($req_verify, array($id_Film, $user_id));
                $count_verify = $result_verify->rowCount();
                $result_verify = $result_verify->fetch();

                if ($count_verify == 0)
                {
                    $req = "INSERT INTO vote SET id_Film = ? AND id_Membre = ? AND a_like = 1";
                    $this->DB->insert($req, array($id_Film, $user_id));
                }
                else{
                    $islike = $result_verify['a_like'];
                    $isunlike = $result_verify['an_unlike'];

                    if($islike == 0 && $isunlike == -1)
                    {
                        $req = "UPDATE vote set a_like = 1 AND an_unlike = 0 WHERE id_Film = ? AND id_Membre = ?";
                        $this->DB->update($req, array($id_Film, $user_id));
                    }
                }
                  
            }

         }

         public function unlike($ref, $id_Film, $user_id)
         {
            $req = "SELECT * FROM $ref WHERE id = ?";
            $request = $this->DB->query($req, array($id_Film));

            if($request->rowCount() > 0)
            {
                $req_verify = "SELECT * FROM vote WHERE id_Film = ? AND id_Membre = ?";
                $result_verify = $this->DB->query($req_verify, array($id_Film, $user_id));
                $count_verify = $result_verify->rowCount();
                $result_verify = $result_verify->fetch();

                if ($count_verify == 0)
                {
                    $req = "INSERT INTO vote SET id_Film = ? AND id_Membre = ? AND an_unlike = -1";
                    $this->DB->insert($req, array($id_Film, $user_id));
                }
                else{
                    $islike = $result_verify['a_like'];
                    $isunlike = $result_verify['an_unlike'];

                    if($islike == 1 && $isunlike == 0)
                    {
                        $req = "UPDATE vote set a_like = 0 AND an_unlike = -1 WHERE id_Film = ? AND id_Membre = ?";
                        $this->DB->update($req, array($id_Film, $user_id));
                    }
                }
                  
            }
         }

         /**
          *  permet d'ajouter une classe is-liked ou dis-liked suivant la base de données
          */
         public static function getclass($vote)
         {
                if($vote)
                {
                    return $vote->vote == 1 ? 'is_liked' : 'is_disliked';
                }

                return null;
         }


     }

?>