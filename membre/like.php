<?php  

        require '../database.php';

        if($_SERVER['REQUEST_METHOD'] != 'POST')
        {
            http_response_code(403);
            die();
        }

        require 'vote.php';

        $vote = new Vote($DB);

        if($_GET['a_like'] == 1)
        {
            $vote->like('film', $_GET['id_Film'], $_SESSION['id']);
        }
        else
        {
            $vote->unlike('film',$_GET['id_Film'], $_SESSION['id']);
        }


        header("Location: jeVote.php?id=" .$_GET['id_Film']);
?>
