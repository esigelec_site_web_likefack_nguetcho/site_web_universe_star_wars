<?php 
    session_start();
    include ('../database.php');
    require 'vote.php';

    $vote = null;
   
    $request = "SELECT * FROM film";
    $result=$DB->query($request, array());

    while($ligne=$result->fetch())
    {
?>
        <img width = '300' height = '280' src="<?php echo $ligne['image_Film']; ?>"><br/>
        <?php echo $ligne['title']; ?><br/>

        <?php  
            $req_vote = "SELECT * FROM vote WHERE id_Film = ?";
            $result_vote = $DB->query($req_vote, array($ligne['id']));

            $req_like = "SELECT * FROM vote WHERE id_Film = ? AND a_like = ?";
            $req_unlike = "SELECT * FROM vote WHERE id_Film = ? AND an_unlike = ?";

            $result_like = $DB->query($req_like, array($ligne['id'], 1));
            $result_unlike = $DB->query($req_unlike, array($ligne['id'], -1));

            $count_like = $result_like->rowCount();
            $count_unlike = $result_unlike->rowCount();
            $count_vote= $result_vote->rowCount();

           if($count_like + $count_unlike == 0)
           {
               $average = 100;
               echo $average;
           }
           else
           {
               $average = round(100 * ($count_vote / ($count_like + $count_unlike)));
               echo $average;
           }
        ?>

        <?php  
                 if(isset($_SESSION['id']))
                 {
                     $req = "SELECT * FROM vote WHERE id_Film = ? AND id_Membre = ?";
                     $result_vote = $DB->query($req, array($ligne['id'], $_SESSION['id']));
                     $result_vote = $result_vote->fetch();
                     if($result_vote['a_like'] == 1 && $result_vote['an_unlike'] == 0)
                     {
                         $vote = $result_vote['a_like'];
                     }
                     elseif($result_vote['a_like'] == 0 && $result_vote['an_unlike'] == -1)
                     {
                         $vote = $result_vote['an_unlike'];
                     }
                     else{
                         $vote = null;
                     }
                 }
           
        ?>

        <div class="vote <?= Vote::getclass($vote); ?>">
            <div class="vote_bar">
                <div class="vote_progress" style="width: <?= $average ?>;"></div>
            </div>
            

            <div class="vote_btn">

                <form method="POST">
                     <?php $name = $ligne['id']; ?>
                    <button type="submit" name="like" value="<?= $name ?>" class="btn_like"><i class="fa-fa-thumbs-up"></i><?= $count_like ?></button>
                </form>
                
                <form method="POST">
                    <?php $name = $ligne['id']; ?>
                   <button type="submit" name="unlike" value="<?= $name ?>" class="btn_unlike"><i class="fa-fa-thumbs-down"></i><?= $count_unlike ?></button>
                </form>
            </div>

            <div>
                <form method="POST">  
                    <p>Commentaires</p></br>
                    <?php  
							if (isset($err_com)) {
						?>

							<div><?= $err_com ?></div>

						<?php } ?>
                     <input type="text" placeholder="Commentaire...." name="commentaire">
                    <button type="submit" name="send">Enregistrer</button>

                </form>

                 <?php 

                        $new_vote = new Vote($DB);

                        if(!empty($_POST))
                        {
                            extract($_POST);

                            if(isset($_POST['like']))
                            {
                                $id_Film = $_POST['like'];
                                $new_vote->like('film', $id_Film, $_SESSION['id']);
                            }

                            else if(isset($_POST['dislike']))
                            {
                                $id_Film = $_POST['dislike'];
                                $new_vote->unlike('film', $id_Film, $_SESSION['id']);
                            }

                            if(isset($_POST['send']))
                            {
                                $commentaire = htmlentities(trim($commentaire));
                                
                                if(empty($commentaire))
                                {
                                    $err_com = "Vous devez commenter avant de valider";
                                }
                                else{
                                    
                                    $req = "SELECT * FROM vote WHERE id_Film = ? AND id_Membre = ?";
                                    $result = $DB->query($req, $id_Film, $_SESSION['id']);
                                    $count = $result->rowCount();
                                   
                                    if($count == 0)
                                    {
                                        $req_comment = "INSERT INTO vote SET id_Film = ?, id_Membre  = ?, commentaire = ?";
                                        $DB->insert($req_comment, $id_Film, $_SESSION['id'], $commentaire);
                                    }

                                    else{
                                        $req_comment = "UPDATE vote SET commentaire = ? WHERE id_Film = ? AND id_Membre = ?";
                                        $DB->update($req_comment, array($commentaire, $id_Film, $_SESSION['id']));
                                    }

                                }
                            }

                        }    
                    
                    ?>

            </div>

        </div>

    <?php } ?>