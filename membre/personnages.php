<!DOCTYPE html>
<html>
<head>
	<title>Accueil Club</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
	<link href="https://fonts.googleapis.com/css2?family=Vampiro+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Concert+One&family=Vampiro+One&display=swap" rel="stylesheet">
</head>

<body>

	<!-- En-tête du site -->
		<header>
			<div class="header">
				<p class="police-en-tete">Embarquons dans l'univers Star Wars</p>		
			</div>


			<!-- slider d'images -->

			<div class="slider">
				<div class="slides">
					<div class="slide"><img src="img/en-tete.jpg" height="250" width="300"></div>
					<div class="slide"><img src="img/logo.jpg" width="300" height="250"></div>
					<div class="slide"><img src="img/OIP2.jpg" width="400" height="250"></div>
					<div class="slide"><img src="img/telecharger.jpg" width="400" height="250"></div>
					<div class="slide"><img src="img/ogimage.img.jpg" width="400" height="250"></div>
				</div>
			</div>

		</header>

	<!-- Menu de navigation -->
	
	<div class="container">

		<div class="navigation_menu">
			
			<nav class="menu">
				<ul>
					<li><a href="membre/film.php">Films</a> </li>
					<li><a href="membre/personnages.php">Personnages</a></li>
					<li><a href="membre/jeVote.php">Je vote</a></li>
					<li><a>Mon compte 'Star Wars'</a>
						<ul>
							<li>
								  <a href="../deconnexion.php">déconnexion</a>
							</li>
							<li>
								  <a href="monCompte.php">mes Informations</a>
							</li>
						</ul>

					</li>
				</ul>
			</nav>

		</div>

		
		<div class="centerBox">

		<div class="listPersonnage">
						
						<?php

						session_start();
						include ('../database.php');

								if (isset($_POST['submit'])) {
									
									extract($_POST);

									if (empty($recherche)) {
										$err_rch = "Quel est le nom du personnage ?";
									}

									else
									{
										$requestSelect = "SELECT * FROM people WHERE name = ?";
									}

								}

								else
								{
									$requestSelect="Select * from people";
								}

								$result=$DB->query($requestSelect, array($recherche));
								while ($ligne=$result->fetch()) {
						?>

								<div class="affichePersonnage">
									<img src="<?php echo $ligne['image'] ?>">
									<?php echo $ligne['name']; ?><br/>
									<?php echo $ligne['gender']; ?><br/>
									<?php echo "homeworld: " .$ligne['homeworld']; ?>
								</div>

								<?php } ?>


				</div>

				<div class="recherche">

					<form>
						<input class="recherche" type="text" name="recherche" placeholder="Recherche...">
						

						<?php  
							if (isset($err_rch)) {
						?>

							<script type="text"> alert(<?= $err_rch ?>)</script>

						<?php } ?>
						<input class="btFind" type="submit" name="submit" value="Recherche">
					</form>
					
				</div>

				

		</div>

	</div>

	<!-- Pied de page --> 
		<footer> 
			
		</footer>
</body>
</html>