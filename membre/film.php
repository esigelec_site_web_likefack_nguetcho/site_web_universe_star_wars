<!DOCTYPE html>
<html>
<head>
	<title>Accueil Club</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
	<link href="https://fonts.googleapis.com/css2?family=Vampiro+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Concert+One&family=Vampiro+One&display=swap" rel="stylesheet">
</head>

<body>

	<!-- En-tête du site -->
		<header>
			<div class="header">
				<p class="police-en-tete">Embarquons dans l'univers Star Wars</p>		
			</div>


			<!-- slider d'images -->

			<div class="slider">
				<div class="slides">
					<div class="slide"><img src="../img/en-tete.jpg" height="250" width="300"></div>
					<div class="slide"><img src="../img/logo.jpg" width="300" height="250"></div>
					<div class="slide"><img src="../img/OIP2.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/telecharger.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/ogimage.img.jpg" width="400" height="250"></div>
				</div>
			</div>

		</header>

	<!-- Menu de navigation -->
	
	<div class="container">

		<div class="navigation_menu">
			
			<nav class="menu">
				<ul>
					<li><a href="film.php">Film</a></li>
					<li><a href="personnages.php">Personnages</a></li>
					<li><a href="jeVote.php">Je vote</a></li>
					<li><a>Mon compte 'Star Wars'</a>
						<ul>
							<li>
								  <a href="../deconnexion.php">déconnexion</a>
							</li>
							<li>
								  <a href="monCompte.php">mes Informations</a>
							</li>
						</ul>

					</li>
				</ul>
			</nav>

		</div>

		
		<div class="centerBox">

		<div class="listFilm">
						
						<?php

							session_start();
							include ('../database.php');
							define('PER_PAGE', 20);

								if (isset($_SESSION['id'])) {
									header("Location: ../Connexion.php");
									exit;
								}


								if (!empty($_POST)) {
									extract($_POST);


										if (isset($_POST['submit'])) {
									
											$recherche = htmlentities(trim($recherche));

											if (empty($recherche)) {
											$err_rch = "Que voulez-vous rechercher ?";
											}
											else
											{
												 $requestSelect = "SELECT * FROM film WHERE title = ?";
											}
										}

										else
										{
											$query_count = "SELECT COUNT(id) as count FROM film";
											$result_count = $DB->query($query_count);
											$count = (int)$result_count->fetch()['count'];
											$pages = ceil($count / PER_PAGE);
											// pagination
											$page = (int)($_GET['p']);
											$offset = ($page - 1) * PER_PAGE;
											$requestSelect="SELECT * FROM film";
											$requestSelect .=" LIMIT " . PER_PAGE . "OFFSET" .$offset;
										}

										$result=$DB->query($requestSelect, array($recherche));
			

								while ($ligne=$result->fetch()) { 
								?>

								<div class="afficheFilm">
									<img src="<?php echo $ligne['image_Film'] ?>"><br/>
									<?php echo $ligne['title']; ?><br/>
									<?php echo "episode : " .$ligne['episode']; ?><br/>
									<?php echo $ligne['release_date']; ?>
									<?php echo $ligne['description']; ?>
								</div>

								<?php } ?>

								<?php if($pages > 1 && $page< $pages): ?>
									<a href="?<?= URLHelper::params("p", $page + 1 ) ?>">Page suivante</a>
								<?php endif ?>

								<?php if($pages > 1 && $page > 1): ?>
									<a href="?<?= URLHelper::params("p", $page - 1) ?>">Page précédente</a>
								<?php endif ?>
							<?php } ?>

				</div>

				<div class="recherche">

					<form method="POST">

							<?php
				
							if (isset($err_name)) {
								?>	
									<div><?= $err_name ?></div>
								<?php
									}
								?>
						<input type="text" name="recherche" placeholder="Recherche...">
						
						<input class="btFind" type="submit" name="submit" value="Recherche">
					</form>
					
				</div>

		</div>

	</div>
	</body>
</html>