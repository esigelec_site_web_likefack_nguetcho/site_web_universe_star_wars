<!DOCTYPE html>
<html>
<head>
	<title>Accueil Club</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
	<link href="https://fonts.googleapis.com/css2?family=Vampiro+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Concert+One&family=Vampiro+One&display=swap" rel="stylesheet">
</head>
<body>

	<!-- En-tête du site -->
		<header>
			<div class="header">
				<p class="police-en-tete">Embarquons dans l'univers Star Wars</p>		
			</div>


			<!-- slider d'images -->

			<div class="slider">
				<div class="slides">
					<div class="slide"><img src="../img/en-tete.jpg" height="250" width="300"></div>
					<div class="slide"><img src="../img/logo.jpg" width="300" height="250"></div>
					<div class="slide"><img src="../img/OIP2.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/telecharger.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/ogimage.img.jpg" width="400" height="250"></div>
				</div>
			</div>

		</header>

	<!-- Menu de navigation -->
	
	<div class="container">

		<div class="navigation_menu">
			
			<nav class="menu">
				<ul>
					<li><a>Mon compte 'Star Wars'</a>
						<ul>
							<li>
								  <a href="Connexion.php">déconnexion</a>
							</li>
						</ul>

					</li>
				</ul>
			</nav>

		</div>

		
		<div class="centerBox">

				<h1>Présentation du site</h1>

				<div class="presentationSite">
					
						<p>
							Nous vous souhaitons la bienvenue dans un univers créé spécialement pour tous les fans de la franchise starWars.
							Ici vous trouverez l'ensemble des sélections liées à la franchise (Nouvelles sorties films, personnages, informations sur les planètes de la saga ainsi que les appareils, ........).

							Etant déjà un adhérent au groupe, vous aurez accès à toutes liées informantions et vous aurez également la possibilité de partager vos 	avis et de voter.
						</p>

						<p>
							Prenez donc position et explorez ce monde !!!!!
						</p>

						<div>
							<iframe width="560" height="315" src="https://www.youtube.com/embed/QkEZhioqAF0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
				</div>

				<div class="actualitePersonnage">
					
					<h2>Principaux acteurs</h2>

						<?php
								include '../database.php';

								$requestSelect="Select * from people";
								$result=$DB->query($requestSelect);
							while ($ligne=$result->fetch()) {
						?>

								<div class="affichePersonnage">
									<img src="<?php echo $ligne['image']; ?>" width="300" height="200">
									<?php echo $ligne['name']; ?><br/>
								</div>

							<?php } ?>

							<iframe width="560" height="315" src="https://www.youtube.com/embed/8Qn_spdM5Zg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>


				<div class="actualiteFilm">

					<h2>Vos films adorés</h2>
					<?php
									
							$requestSelect="Select * from film";
							$result=$DB->query($requestSelect);
							while ($ligne=$result->fetch()) {

					?>

								<div class="afficheFilm">
									<a href="Inscription.php"><img src="<?php echo $ligne['image_Film']; ?>" width="300" height="200"></a><br/>
									<?php echo $ligne['title']; ?><br/>
									<?php echo "episode : " .$ligne['episode']; ?><br/>
									<?php echo $ligne['release_date']; ?>
								</div>

					<?php } ?>
					

				</div>

		</div>

	</div>

	<!-- Pied de page --> 
		<footer> 
		
		</footer>
</body>
</html>