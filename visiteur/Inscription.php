<!DOCTYPE html>
<html>
<head>
	<title>Unisers Star wars</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
</head>
<body>

	<?php
		session_start();
		include '../database.php';//fichier connexion à la BDD

		if (!empty($_POST)) {
			
				extract($_POST);
				$isValid = true;


				if (isset($_POST['formsend'])) {
					
						//je prends en valeurs les valeurs entrées
						$name = htmlentities(trim($name));
						$pseudo = htmlentities(trim($pseudo));
						$email = htmlentities(trim($email));
						$password = trim($password);
						$cpassword = trim($cpassword);

						//vérification du nom
						if (empty($name)) {
							$isValid = false;
							$err_name = ("Le nom de l'utilisateur doit être rempli");
						}

						//vérification du pseudo
						if (empty($pseudo)) {
							$isValid = false;
							$err_pseudo = ("Veuillez entrer  votre pseudo");
						}

						//vérification de l'email
						if (empty($email)) {
							$isValid = false;
							$err_email = ("L'email de l'utilisateur est requis");
						}
						//on verifie que le format est correct
						elseif (!preg_match("/^[a-z0-9\-_.]+@[a-z]+\.[a-z]{2,3}$/i", $email)) {
							$isValid = false;
							$err_email = "le format du mail  est incorrect";
						}

						else
						{
								//on vérifie que l'email et le pseudo ne sont pas déjà utilisé par un ou d'autres utilisateurs

								$req_rch = "SELECT email FROM user WHERE email = ?";
								$req_mail = $DB->query($req_rch, array($email));
								$req_mail = $req_mail->fetch();

								$req_rch = "SELECT pseudo FROM user WHERE pseudo = ?";
								$req_pseudo = $DB->query($req_rch, array($pseudo));
								$req_pseudo = $req_pseudo->fetch();

								if ($req_mail['email'] <> "") {
									$isValid = false;
									$err_email = "Ce mail existe déjà";
								}

								if ($req_pseudo['pseudo'] <> "") {
									$isValid = false;
									$err_email = "Ce pseudo est déjà utilisé";
								}

						}


						//vérification du mot de passe
						if (empty($password)) {
							$isValid = false;
							$err_pass = ("Le mot de passe ne peut être vide");
						}

						//vérification du prénom
						elseif (empty($cpassword) || ($cpassword != $password)) {
							$isValid = false;
							$err_pass= ("La confirmation du mot de passe est différente");
						}


						if ($isValid) {
							
							$option = [
								'cost' => 12,
							];

							$hashpass = password_hash($password, PASSWORD_BCRYPT, $option);
							$statut = "Inscrit";

							//on insert les données entrées par l'utilisateur

							$req_insert = "INSERT INTO user (name, pseudo, email, statut, password) VALUES (?, ?, ?, ?, ?)";
							$DB->insert($req_insert, array($name, $pseudo, $email, $statut, $hashpass));
							
							header("Location: ../accueil_Adhérent.php");
							exit;
						}
				}


		}
	?>

<form method="POST">
				<?php
				
					if (isset($err_name)) {
					?>
						<div><?= $err_name ?></div>
					<?php
					}
				?>
		<input type="text" name="name" placeholder="Entrer votre nom" value="<?php if(isset($name)){ echo $name; }?>" required> 
				<?php
				
					if (isset($err_pseudo)) {
					?>
						<div><?= $err_pseudo ?></div>
					<?php
					}
				?>
		<input type="text" name="pseudo" placeholder="Entrer votre pseudo" value="<?php if(isset($pseudo)){ echo $pseudo; }?>" required> 
		<?php
				
					if (isset($err_email)) {
					?>
						<div><?= $err_email ?></div>
					<?php
					}
				?>
		<input type="text" name="email" placeholder="Entrer votre email" value="<?php if(isset($email)){ echo $email; }?>" required> 
			<?php
				
					if (isset($err_pass)) {
					?>
						<div><?= $err_pass ?></div>
					<?php
					}
				?>
		<input type="password" name="password" placeholder="Entrer votre mot de passe" value="<?php if(isset($password)){ echo $password; }?>" required> 
		<input type="password" name="cpassword" placeholder="Confirmer votre mot de passe" required> 
		<input type="submit" name="formsend" value="Inscription"> 
	</form>

	</body>
</html>