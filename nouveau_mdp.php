<form method="POST">
    <input type="text" name="n_mdp" placeholder="Entrer votre nouveau mot de passe" require>
</form> 

<?php 

    include 'database.php';
    require 'send_mail.php';
    require 'mdp_oublie.php';

    if(isset($_SESSION['id']))
    {
        header("location: Connexion.php");
        exit;
    }

    if(!empty($_POST))
    {
        extract($_POST);
        
        if(!empty($_POST['n_mdp']))
        {
            $req_update = "UPDATE user SET password = ? WHERE email = ?";
            $DB->update($req_update, array($_POST['email']));

            $from_mail = "ne-pas-repondre@esigelec.com";
            $subject = "Re:nouveau mot de passe";
            $body = "Le mot de passe a été mis à jour. <a href='Connexion.php'>Connectez-vous</a>";
            $mail_sent = new send_mail($from_mail);
            $mail_sent->send($to, $subject, $body);
        }
    }
    

?>