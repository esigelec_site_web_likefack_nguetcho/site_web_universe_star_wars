<!DOCTYPE html>
<html>
<head>
	<title>deleteFilm</title>
</head>
<body>
 
 <?php
 		session_start();
 		include ('../database.php');

 		if (isset($_SESSION['id'])) {
 			header("Location: gestion.php");
 			exit;
 		}

 		if (!empty($_POST)) {
 			extract($_POST);
			$isValid = true;

			if (isset($_POST['submit'])) {

 				$title = htmlentities(trim($title));
 				$episode = htmlentities(trim($episode));

 				if (empty($title)) {
 					$err_title = "Entrer le titre du film";
 					$isValid = false;
 				}

 				elseif (empty($episode)) {
 					$err_episode = "Entrer l'épisode du film";
 					$isValid = false;
 				}

 				elseif (!empty($title) && !empty($episode)) {
 						$req = "SELECT * FROM film WHERE title = ? AND episode = ?";
 						$result = $DB->query($req, array($title, $episode));
 						$result = $result->fetch();
 		
 				}

 				if ($result['title'] == "") { // on vérifie que le nom du personnage existe
 						$err_title = "Aucun titre de film ne correspond à votre recherche ";
 						$isValid = false;
 					}

 				if ($isValid) {
 					$request = "DELETE FROM film WHERE title = ? AND episode = ?";
 					$DB->delete($request, array($title, $episode));

 					header("Location: gestion.php");
 					exit;
 				}
 			}
 		}

 ?>

 <form method="POST">

 <?php
				
				if (isset($err_title)) {
			?>
					<div><?= $err_title ?></div>
			<?php
				}
			?>
<input type="text" name="title" placeholder="titre du film" required>
			<?php
	
				if (isset($err_episode)) {
			?>
					<div><?= $err_episode ?></div>
			<?php
				}
			?>
<input type="text" name="episode" placeholder="numero de l'épisode" required>
			
<input type="submit" name="submit" value="Rechercher et supprimer">

 </form>

</body>
</html>