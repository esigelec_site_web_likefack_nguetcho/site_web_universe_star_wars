<!DOCTYPE html>
<html>
<head>
	<title>ModifPersonnage</title>
</head>
<body>

	<?php  
		session_start();
		include ('../database.php');

		if (isset($_SESSION['id'])) {
			header("Location: ../Connexion.php");
			exit;
		}

		if (!empty($_POST)) {

				extract($_POST);
				$isValid = true;


					if (isset($_POST['recherche'])) {

			$rch_name = htmlentities(trim($rch_name));
			
			if (empty($title)) {
				$err_name = "Vous devez entrer le nom du personnage";
				$isValid = false;
			}

			else
			{
				$request = "SELECT * FROM people WHERE name = ?";
				$result = $DB->query($request, array($name));
				$result = $result->fetch();

				if (empty($result['name'])) {
					$err_name = "Aucun n'élément trouvé";
					$isValid = false;
				}

				else
				{ ?>

					<form method="POST" enctype="multipart/form-data">

					<?php
				
						if (isset($err_name)) {
						?>
								<div><?= $err_name ?></div>
						<?php
							}
						?>
					<input type="text" name="name" value="<?= $result['name'] ?>">;
						<?php
				
							if (isset($err_gender)) {
						?>
								<div><?= $err_gender ?></div>
						<?php
							}
						?>
					<input type="text" name="gender" value="<?= $result['gender'] ?>">;
							<?php
				
							if (isset($err_homeworld)) {
						?>
								<div><?= $err_homeworld ?></div>
						<?php
							}
						?>							
					<input type="text" name="homeworld" value="<?= $result['homeworld'] ?>">;

					<input type="file" name="image" value="<?= $result['image'] ?>" class="form-control-file">;

					<input type="submit" name="modif" value="Modifier">;

					</form>

								
				<?php } ?>

			<?php 

				if (isset($_POST['modif'])) {

						$name = htmlentities(trim($name));
						$gender = htmlentities(trim($gender));
						$homeworld = htmlentities(trim($homeworld));
						$image = $_FILES['image']['name'];
						$upload = "../img/".$image;
						move_uploaded_file($_FILES['image']['tmp_name'], $upload);
						$valid = true;

					if (empty($name)) {
						$err_name = "cet espace ne peut être vide";
						$Valid = false;
					}

					if (empty($gender)) {
						$err_entitled = "Quel est le genre du personnage ?";
						$Valid = false;
					}

					if (empty($homeworld)) {
						$err_date = "homeworld requis";
						$Valid = false;
					}

					if ($Valid) {
					
							$request = "UPDATE people SET name = ?, gender = ?, homeworld = ?, image = ?";
							$DB->update($request, array($name, $gender, $homeworld, $upload));
					}
				}
			?>
				
			<?php } ?>
		<?php } ?>
			
		<?php } ?>

		<form method="POST">
		<input type="text" name="rch_name" placeholder="Nom du personnage">
					<?php
				
							if (isset($err_name)) {
						?>
								<div><?= $err_name ?></div>
						// <?php
							}
						?>
		<input type="submit" name="recherche" value="recherche">
	</form>

</body>
</html>