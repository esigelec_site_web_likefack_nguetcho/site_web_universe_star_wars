<!DOCTYPE html>
<html>
<head>
	<title>ModifPlanet</title>
</head>
<body>

	<?php  
		session_start();
		include ('../database.php');

		if (isset($_SESSION['id'])) {
			header("Location: ../Connexion.php");
			exit;
		}

		if (!empty($_POST)) {
			extract($_POST);
			$isValid = true;

				if (isset($_POST['recherche'])) {

			$rch_name = htmlentities(trim($rch_name));
			
			if (empty($title)) {
				$err_name = "Vous devez entrer le nom de la planète";
				$isValid = false;
			}

			else
			{
				$request = "SELECT * FROM planet WHERE name = ?";
				$result = $DB->query($request, array($name));
				$result = $result->fetch();

				if (empty($result['name'])) {
					$err_name = "Aucun n'élément trouvé";
					$isValid = false;
				}

				else
				{ ?>


					<form method="POST" enctype="multipart/form-data">


					</form>

							<?php
				
						if (isset($err_name)) {
						?>
								<div><?= $err_name ?></div>
						<?php
							}
						?>
					<input type="text" name="name" value="<?= $result['name'] ?>">;
						
						<?php
				
							if (isset($err_diameter)) {
						?>
								<div><?= $err_diameter ?></div>
						<?php
							}
						?>
					<input type="text" name="diameter" value="<?= $result['diameter'] ?>">;
						<?php
				
							if (isset($err_population)) {
						?>
								<div><?= $err_population ?></div>
						<?php
							}
						?>							
					<input type="text" name="population" value="<?= $result['population'] ?>">;
							
					<input type="file" name="image" value="<?= $result['image'] ?>" class="form-control-file"> ;

					<input type="submit" name="modif" value="Modifier">;
				<?php } ?>


			<?php 


				if (isset($_POST['modif'])) {
					$name = htmlentities(trim($name));
					$diameter = htmlentities(trim($diameter));
					$population = htmlentities(trim($population));
					$image = $_FILES['image']['name'];
					$upload = "../img/".$image;
					move_uploaded_file($_FILES['image']['tmp_name'], $upload);
					$valid = true;

					if (empty($name)) {
						$err_name = "cet espace ne peut être vide";
						$Valid = false;
					}

					if (empty($diameter)) {
						$err_diameter = "Veuilez saisir le diamètre de la planète";
						$Valid = false;
					}

					if (empty($population)) {
						$err_population = "le nombre d'habitants sur la planète";
						$Valid = false;
					}

					if (empty($image)) {
						$err_image = "il faut une image pour cette planète";
					}

					if ($Valid) {
						$request = "UPDATE planet SET name = ?, diameter = ?, population = ?, image = ?";
						$DB->update($request, array($name, $diameter, $population, $upload));
					}	
				}

				?>
			
			<?php } ?>
		<?php } ?>
	<?php } ?>

	<form method="POST">

					<?php
				
							if (isset($err_name)) {
						?>
								<div><?= $err_name ?></div>
						<?php
							}
						?>
		<input type="text" name="rch_planet" placeholder="Nom de la planète">
		<input type="submit" name="recherche" value="recherche">
	</form>

</body>
</html>