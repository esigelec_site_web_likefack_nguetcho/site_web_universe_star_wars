<!DOCTYPE html>
<html>
<head>
	<title>ModifEvent</title>
</head>
<body>

	<?php  
		session_start();
		include ('../database.php');

		if (isset($_SESSION['id'])) {
			header("Location: gestion.php");
			exit;
		}

		if (!empty($_POST)) {
				
				extract($_POST);
				$isValid = true;

				if (isset($_POST['recherche'])) {
		
				$title = htmlentities(trim($title));	

					if (empty($title)) {
						$err_title = "Vous devez entrer le titre de l'évènement pour avoir toutes les informations";
						$isValid = false;
					}

					else
					{
						$request = "SELECT * FROM event WHERE title = ?";
						$result = $DB->query($request, array($title));
						$result = $result->fetch();

						if (empty($result['title'])) {
							$err_title = "Aucun n'élément trouvé";
							$isValid = false;
						}

						else
						{ ?>

							<?php
								if (isset($err_title)) {
								?>
									<div><?= $err_title ?></div>
								<?php
									}
								?>
								<input type="text" name="title" value="<?= $result['title'] ?>">;
									<?php

										if (isset($err_date)) {
										?>
											<div><?= $err_date ?></div>
										<?php
										}
									?>
								<input type="text" name="date" value="<?= $result['date'] ?>">;
								<?php
					
										if (isset($err_entitled)) {
										?>
											<div><?= $err_entitled ?></div>
										<?php
										}
									?>							
								<input type="text" name="entitled" value="<?= $result['entitled'] ?>">;

								<input type="submit" name="modif" value="Modifier">;
					
						<?php } ?>
					
					<?php } ?>

				<?php 


					if (isset($_POST['modif'])) {
							$title = htmlentities(trim($title));
							$date = htmlentities(trim($date));
							$entitled = htmlentities(trim($entitled));
							$valid = true;

							if (empty($title)) {
								$err_title = "Vous devez entrer le titre de l'évènement pour avoir toutes les informations";
								$Valid = false;
							}

							if (empty($entitled)) {
								$err_entitled = "Veuilez saisir le contenu de l'évènement";
								$Valid = false;
							}

							if (empty($date)) {
								$err_date = "Vous devez entrer la date de l'évènement";
								$Valid = false;
							}

							if ($Valid) {
									$request = "UPDATE event SET entitled = ?, title = ?, date = ?";
									$DB->update($request, array($entitled, $title, $date));
								}	
					}
				?>
		
		<?php } ?>

	<?php } ?>

	<form method="POST">
				<?php
				
							if (isset($err_title)) {
						?>
								<div><?= $err_title ?></div>
						<?php
							}
						?>
		<input type="text" name="title" placeholder="Titre de l'évènement">
					
		<input type="submit" name="recherche" value="recherche">
	</form>
	
</body>

</html>