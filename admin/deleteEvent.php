<!DOCTYPE html>
<html>
<head>
	<title>deleteEvent</title>
</head>
<body>

 <?php
 		session_start();
 		include ('../database.php');

 		if (isset($_SESSION['id'])) {
 			header('Location: gestion.php');
 			exit;
 		}

 		if (!empty($_POST)) {
 			extract($_POST);
			$isValid = true;

					if (isset($_POST['submit'])) {

 					$title = htmlentities(trim($title));
 					$date = htmlentities(trim($date));

 				if (empty($title)) {
 					$err_title = "Entrer le titre de l'évènement";
 					$isValid = false;
 				}

 				elseif (empty($date)) {
 					$err_date = "Entrer la date de l'évènement";
 					$isValid = false;
 				}

 				elseif (!empty($title) && !empty($date)) {
 						$req = "SELECT * FROM event WHERE title = ? AND date = ?";
 						$result = $DB->query($req, array($title, $date));
 						$result = $result->fetch();
 		
 				}

 				if ($result['title'] == "") { // on vérifie que le nom du personnage existe
 						$err_title = "l'évènement n'a pas été trouvé ";
 						$isValid = false;
 					}

 				if ($isValid) {
 					$request = "DELETE FROM event WHERE title = ? AND date = ?";
 					$DB->delete($request, array($title, $date));

 					header("Location: gestion.php");
 					exit;
 				}
 		}
 		}

 ?>

 <form method="POST">
		 
 
 <?php
				
				if (isset($err_title)) {
			?>
					<div><?= $err_title ?></div>
			<?php
				}
			?>
<input type="text" name="title" placeholder="titre de l'évènement" required>
			<?php
	
				if (isset($err_date)) {
			?>
					<div><?= $err_date ?></div>
			<?php
				}
			?>
<input type="text" name="date" placeholder="date de l'évènement" required>	
<input type="submit" name="submit" value="Rechercher et supprimer">

 </form>

</body>
</html>