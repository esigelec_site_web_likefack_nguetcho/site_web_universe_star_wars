<!DOCTYPE html>
<html>
<head>
	<title>ajoutFilm</title>
</head>
<body>

		<?php  
			session_start();
			include ('../database.php');
			
			if (isset($_SESSION['id'])) {
				header("Location: ../Connexion.php");
				exit;
			}

			if (!empty($_POST)) {
					
					extract($_POST);	
					$isValid = true;
					$_image = null;


					if (isset($_POST['send'])) {

					$title = htmlentities(trim($title));
					$episode = htmlentities(trim($episode));
					
					// telecharger une image
					$_image = $_FILES['image']['name'];
					$upload = "../img/".$_image;

					move_uploaded_file($_FILES['image']['tmp_name'], $upload);

					$release_date = htmlentities(trim($release_date));
					$description = htmlentities(trim($description));

					
					if (empty($title)) {
						$err_title = "Quel est le titre du film ?";
						$isValid = false;
					}

					if (empty($episode)) {
						$err_episode = "Vous devez entrer l'episode";
						$isValid = false;
					}

					if (empty($release_date)) {
						$err_release_date = "Quel est la date de sortie du film ?";
						$isValid + false;
					}

					if (empty($description)) {
						$err_description = "Veuillez décrire le film";
						$isValid + false;
					}

					if ($isValid) {
						$request = "INSERT INTO film (title, episode, release_date, image_Film, description_Film) VALUES (?, ?, ?, ?, ?)";
						$DB->insert($request, array($title, $episode, $release_date, $upload, $description));

						header("Location: gestion.php");
						exit;
					}

				}
			}
				
		?>

<form method="POST" enctype="multipart/form-data">

<?php
			
			if (isset($err_title)) {
		?>
				<div><?= $err_title ?></div>
		<?php
			}
		?>
<input type="text" name="title" placeholder="Titre du nouveau film" required><br/>
		<?php

			if (isset($err_episode)) {
		?>
				<div><?= $err_episode ?></div>
		<?php
			}
		?>
<input type="int" name="episode" placeholder="numero de l'épisode" required><br/>
		
<input type="file" name="image" class="form-control-file"><br/>
		<?php

			if (isset($err_release_date)) {
		?>
				<div><?= $err_release_date ?></div>
		<?php
			}
		?>
<input type="text" name="release_date" placeholder="date de sortie" required><br/>
		<?php

			if (isset($err_description)) {
		?>
				<div><?= $err_description ?></div>
		<?php
			}
		?>
<input type="text" name="description" placeholder="description du film" required><br/>			
<input type="submit" name="send" value="Enregistrer">


</form>

</body>
</html>