<!DOCTYPE html>
<html>
<head>
	<title>ModifFilm</title>
</head>
<body>

	<?php  
		session_start();
		include ('../database.php');

		if (isset($_SESSION['id'])) {
			header("Location: gestion.php");
			exit;
		}


		if (!empty($_POST)) {
				
				extract($_POST);
				$isValid = true;


			if (isset($_POST['recherche'])) {

			$title = htmlentities(trim($title));
			
			if (empty($title)) {
				$err_title = "Vous devez entrer le titre du film";
				$isValid = false;
			}

			else
			{
				$request = "SELECT * FROM film WHERE title = ?";
				$result = $DB->query($request, array($title));
				$result = $result->fetch();

				if (empty($result['title'])) {
					$err_title = "Aucun n'élément trouvé";
					$isValid = false;
				}

				else
				{ ?>

				<form method="POST" enctype="multipart/form-data">

				<?php
				
				if (isset($err_title)) {
				?>
						<div><?= $err_title ?></div>
				<?php
					}
				?>

			<input type="text" name="title" value="<?= $result['title'] ?>">;

					<?php
		
					if (isset($err_episode)) {
				?>
						<div><?= $err_episode ?></div>
				<?php
					}
				?>
			<input type="text" name="episode" value="<?= $result['episode'] ?>">;
					<?php
		
					if (isset($err_release_date)) {
				?>
						<div><?= $err_release_date ?></div>
				<?php
					}
				?>
			<input type="text" name="release_date" value="<?= $result['release_date'] ?>">;
					
			<input type="file" name="image" value="<?= $result['image'] ?>" class="form-control-file">;
				<?php
		
					if (isset($err_description)) {
				?>
						<div><?= $err_description ?></div>
				<?php
					}
				?>
			<input type="text" name="description" value="<?= $result['description_Film'] ?>">;
				
			<input type="submit" name="modif" value="Modifier">;

				</form>

				<?php } ?>

					<?php 


						if (isset($_POST['modif'])) {
							
								$title = htmlentities(trim($title));
								$episode = htmlentities(trim($episode));
								$release_date = htmlentities(trim($release_date));
								$image = $_FILES['image']['name'];
								$upload = "../img/".$image;
								move_uploaded_file($_FILES['image']['tmp_name'], $upload);
								$description = htmlentities(trim($description));
								$valid = true;

							if (empty($title)) {
								$err_title = "il faut un titre a ce film";
									$Valid = false;
							}

							if (empty($episode)) {
								$err_episode = "Veuilez saisir l'episode";
								$Valid = false;
							}

							if (empty($release_date)) {
									$err_release_date = "Vous devez entrer la date de sortie";
									$Valid = false;
							}

							if (empty($image)) {
								$err_image = "Il faut une image de couverture";
								$Valid = false;
							}

							if (empty($description)) {
								$err_description = "il faut une description du film";
								$Valid = false;
							}


							if ($Valid) {
				
									$request = "UPDATE film SET title = ?, episode = ?, release_date = ?, image = ?, description_Film = ?";
									$DB->update($request, array($title, $episode, $release_date, $upload, $description));
							}

						}
					?>	
			<?php } ?>
		<?php } ?>

		<?php } ?>

		<form method="POST">

						<?php
				
							if (isset($err_title)) {
						?>
								<div><?= $err_title ?></div>
						<?php
							}
						?>
		<input type="text" name="title" placeholder="Titre de l'évènement">
		<input type="submit" name="recherche" value="recherche">
	</form>

</body>
</html>