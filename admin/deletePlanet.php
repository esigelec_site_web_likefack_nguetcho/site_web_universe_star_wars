<!DOCTYPE html>
<html>
<head>
	<title>deletePersonnage</title>
</head>
<body>
 
 <?php
 		session_start();
 		include ('../database.php');

 		if (isset($_SESSION['id'])) {
 			header("Location: gestion.php");
 			exit;
 		}

 		if (!empty($_POST)) {
 			
 			extract($_POST);
			$isValid = true;

			if (isset($_POST['submit'])) {

 			$name = htmlentities(trim($name));

 				if (empty($name)) {
 					$err_title = "Entrer le nom de la planète à supprimer";
 					$isValid = false;
 				}

 				$req = "SELECT * FROM planet WHERE name = ?";
 				$result = $DB->query($req, array($name));
 				$result = $result->fetch();
 	
 				if (empty($result['name'])) { // on vérifie que le nom du personnage existe
 						$err_name = "Aucune planète de ce nom " .$name. "n'est enregistré dans la base de donnée";
 						$isValid = false;
 					}


 				if ($isValid) {

 						$request = "DELETE FROM planet WHERE name = ?";
 						$DB->delete($request, array($name));

 					header("Location: gestion.php");
 					exit;
 				}
 		}

 		}
 ?>

 <form method="POST">
 <?php
				
				if (isset($err_name)) {
			?>
					<div><?= $err_name ?></div>
			<?php
				}
			?>
<input type="text" name="name" placeholder="Nomination de la planète" required>
			
<input type="submit" name="submit" value="Rechercher et supprimer">

 </form>

