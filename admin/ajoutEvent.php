<!DOCTYPE html>
<html>
<head>
	<title>ajoutEvent</title>
</head>
<body>

		<?php  
				session_start();
				include ('../database.php');

				if (isset($_SESSION['id'])) {
									header("Location: ../Connexion.php");
									exit;
				}

				if (!empty($_POST)) {
	

					extract($_POST);
					$isValid = true;


					if (isset($_POST['submit'])) {

						$title = htmlentities(trim($title));
						$date = htmlentities(trim($date));
						$entitled = htmlentities(trim($entitled));
						
						if (empty($title)) {
							
							$err_title = "Vous devez entrer un titre pour l'évènement";
							$isValid = false;
						}

						if (empty($date)) {
							$err_date = "Veuillez entrer la date de réalisation de l'évènement";
							$isValid = false;
						}

						if (empty($entitled)) 
						{
							$err_entitled = "Veuillez entrer l'intitulé de l'évènement";
							$isValid = false;
							
						}


						if ($isValid) {
							
							$request = "INSERT INTO event (entitled, title, date) VALUES (?, ?, ?)";
							$DB->insert($request, array($entitled, $title, $date));


							header("Location: gestion.php");
							exit;
						}
					}
				}

		?>

		<form method="POST">

		<?php
				
				if (isset($err_title)) {
			?>
					<div><?= $err_title ?></div>
			<?php
				}
			?>
<input type="text" name="title" placeholder="Titre de l'évènement" required><br/>
	
			<?php
	
				if (isset($err_entitled)) {
			?>
					<div><?= $err_entitled ?></div>
			<?php
				}
			?>
<input type="text" name="entitled" placeholder="Intitulé de l'évènement" required><br/>
			
			<?php
	
				if (isset($err_date)) {
			?>
					<div><?= $err_date ?></div>
			<?php
				}
			?>					
<input type="text" name="date" placeholder="Date de réalisation" required><br/>

<input type="submit" name="submit" value="Enregistrer">

		</form>

</body>
</html>