<!DOCTYPE html>
<html>
<head>
	<title>ajoutPersonnage</title>
</head>
<body>

		<?php  
			session_start();
			include ('../database.php');

			if (isset($_SESSION)) { // vérification de connexion à la session actuelle
				header("Location: ../Connexion.php");
				exit;
			}

			if (!empty($_POST)) {
				extract($_POST);
				$isValid = true;


				if (isset($_POST['submit'])) {

				//recupération des entrées de l'utilisateur
					$name = htmlentities(trim($name));
					$gender = $_POST['gender'];
					$image = $_FILES['image']['name'];
					$upload = "../img/".$image;
					move_uploaded_file($_FILES['image']['tmp_name'], $upload);
					$homeworld = htmlentities(trim($homeworld));
					
					if (empty($name)) {
						$err_name = "Nom du personnage requis";
						$isValid = false;
					}

					if (empty($gender)) {
						$err_gender = "Vous devez entrer le genre du personnage";
						$isValid = false;
					}

					if (empty($homeworld)) {
						$err_homeworld = "homeworld requis";
						$isValid + false;
					}

					if ($isValid) {
						$request = "INSERT INTO people (name, gender, homeworld, image) VALUES (?, ?, ?, ?)";
						$DB->insert($request, array($name, $_POST['gender'], $homeworld, $upload));

						header("Location: gestion.php");
						exit;
					}

			}
			}
				
		?>

<form method="POST" enctype="multipart/form-data">

<?php
				
				if (isset($err_name)) {
			?>
					<div><?= $err_name ?></div>
			<?php
				}
			?>
<input type="text" name="name" placeholder="Nom du personnage" required><br/>
			<?php
	
				if (isset($err_gender)) {
			?>
					<div><?= $err_gender ?></div>
			<?php
				}
			?>
<input type="radio" name="gender" value="F"><br/>
<input type="radio" name="gender" value="M"><br/>

			<?php
	
				if (isset($err_homeworld)) {
			?>
					<div><?= $err_homeworld ?></div>
			<?php
				}
			?>
<input type="text" name="homeworld" placeholder="homeworld requis" required><br/>
			
<input type="file" name="image" class="form-control-file" required><br/>
			
<input type="submit" name="submit" value="Enregistrer">

</form>

</body>
</html>