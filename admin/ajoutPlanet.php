<!DOCTYPE html>
<html>
<head>
	<title>ajoutPlanet</title>
</head>
<body>

		<?php  
			session_start();
			include ('../database.php');

			if (isset($_SESSION)) { // vérification de connexion à la session actuelle
				header("location: ../Connexion.php");
				exit;
			}

			if (!empty($_POST)) {
				extract($_POST);
				$isValid = true;

					if (isset($_POST['submit'])) {

				//recupération des entrées de l'utilisateur
				$name = htmlentities(trim($name));
				$diameter = htmlentities(trim($diameter));
				$image = $_FILES['image']['name'];
					$upload = "../img/".$image;
					move_uploaded_file($_FILES['image']['tmp_name'], $upload);
				$population = htmlentities(trim($population));	
					
					if (empty($name)) {
						$err_name = "Nom du personnage requis";
						$isValid = false;
					}

					if (empty($diameter)) {
						$err_diameter = "Vous devez entrer le diamètre de la planète";
						$isValid = false;
					}

					if (empty($image)) {
							$err_image = "Il faudrait une image pour créer le profil du personnage";
							$isValid = false;
					}

					if (empty($population)) {
						$err_homeworld = "la population est requise";
						$isValid + false;
					}

					if ($isValid) {
						$request = "INSERT INTO planet (name, diameter, population, image) VALUES (?, ?, ?, ?)";
						$DB->insert($request, array($name, $diameter, $population, $upload));

						header("Location: gestion.php");
						exit;
					}

			}
			}
				
		?>

<form method="POST" enctype="multipart/form-data">

	
<?php
				
				if (isset($err_name)) {
			?>
					<div><?= $err_name ?></div>
			<?php
				}
			?>
<input type="text" name="name" placeholder="Nom de la planète" required><br/>
			<?php
	
				if (isset($err_diameter)) {
			?>
					<div><?= $err_diameter ?></div>
			<?php
				}
			?>	
<input type="text" name="diameter" placeholder="diamètre de la planète" required><br/>
			<?php
	
				if (isset($err_population)) {
			?>
					<div><?= $err_population ?></div>
			<?php
				}
			?>
<input type="text" name="population" placeholder="information sur le nombre d'habitant requis" required><br/>
			<?php
	
				if (isset($err_image)) {
			?>
					<div><?= $err_image ?></div>
			<?php
				}
			?>
<input type="file" name="image" class="form-control-file" required><br/>

<input type="submit" name="submit" value="Enregistrer">


</form>

</body>
</html>