<!DOCTYPE html>
<html>
<head>
	<title>Accueil Club</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
	<link href="https://fonts.googleapis.com/css2?family=Vampiro+One&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Concert+One&family=Vampiro+One&display=swap" rel="stylesheet">
</head>
<body>

	<!-- En-tête du site -->
		<header>
			<div class="header">
				<p class="police-en-tete">Embarquons dans l'univers Star Wars</p>		
			</div>


			<!-- slider d'images -->

			<div class="slider">
				<div class="slides">
					<div class="slide"><img src="../img/en-tete.jpg" height="250" width="300"></div>
					<div class="slide"><img src="../img/logo.jpg" width="300" height="250"></div>
					<div class="slide"><img src="../img/OIP2.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/telecharger.jpg" width="400" height="250"></div>
					<div class="slide"><img src="../img/ogimage.img.jpg" width="400" height="250"></div>
				</div>
			</div>

		</header>

	<!-- Menu de navigation -->
	
	<div class="container">

		<div class="navigation_menu">
			
			<nav class="menu">
				<ul>
					<li><a>Mise à jour</a>
						<ul>
							<li>
								  <a>Création</a>

								  <ul>
								  		<li>
								  			<a href="ajoutFilm.php">Ajouter un Film</a>
								  		</li>

								  		<li>
								  			<a href="ajoutPersonnage.php">Ajouter un Personnage</a>
								  		</li>

								  		<li>
								  			<a href="ajoutPlanet.php">Ajouter une Planète</a>
								  		</li>

								  		<li>
								  			<a href="ajoutEvent.php">Ajouter un évènement</a>
								  		</li>


								  </ul>
							</li>
							<li>
								  <a>Modification</a>


								  <ul>
								  		<li>
								  			<a href="modifFilm.php">Modifier un Film</a>
								  		</li>

								  		<li>
								  			<a href="modifPersonnage.php">Modifier un Personnage</a>
								  		</li>

								  		<li>
								  			<a href="modifPlanet.php">Modifier une Planète</a>
								  		</li>

								  		<li>
								  			<a href="modifEvent.php">Modifier un évènement</a>
								  		</li>


								  </ul>
							</li>
							<li>
								  <a>suppression</a>

								   <ul>
								  		<li>
								  			<a href="deleteFilm.php">Supprimer un Film</a>
								  		</li>

								  		<li>
								  			<a href="deletePersonnage.php">Supprimer un Personnage</a>
								  		</li>

								  		<li>
								  			<a href="deletePlanet.php">Supprimer une Planète</a>
								  		</li>

								  		<li>
								  			<a href="deleteEvent.php">Supprimer un évènement</a>
								  		</li>


								  </ul>
							</li>
						</ul>

					</li>

					<li>
						<a href="../Connexion.php">Déconnexion</a>
					</li>
				</ul>
			</nav>

		</div>

		
		<div class="centerBox">

				
		</div>

	</div>

	<!-- Pied de page --> 
		<footer> 
			
		</footer>
</body>
</html>