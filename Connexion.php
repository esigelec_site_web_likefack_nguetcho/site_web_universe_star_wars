<!DOCTYPE html>
<html>
<head>
	<title>Unisers Star wars</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/styleCSS/style.css">
</head>
<body class="accueil">

<?php

session_start();
include 'database.php'; // insertion du fichier de connexion à la BDD

	if (isset($_SESSION['id'])) {
		header("visiteur/accueil_visiteur.php");
		exit;
	}

	if (!empty($_POST))
	{
			extract($_POST);
			$isValid = true;

		if (isset($_POST['formsend'])) {
			$email = htmlentities(trim($email));
			$password = trim($password);

			if (empty($email)) { // on vérifie qu'il a renseigné une valeur
				$isValid = false;
				$err_email = "Vous devez entrer votre email";
			}

			if (empty($password)) { //on vérifie le mot de passe 
				$isValid = false;
				$err_pass = "Vous devez remplir un mot de passe";
			}
			else
			{
				$option = [
					'cost' => 12,
				];	

				$hashpass = password_hash($password, PASSWORD_BCRYPT, $option);
				$req_rch = "SELECT * FROM user WHERE email = ? AND password = ?";
				$req = $DB->query($req_rch, array($email, $hashpass));
				$req = $req->fetch();	
			}

			if ($req['id'] == "") { // on vérifie que le compte existe
				$isValid = false;
				$err_email = "email ou mot de passe incorrect";
			}

			if ($isValid) {

				$_SESSION['id'] = $req['id'];
				$_SESSION['email'] = $req['email'];
				$_SESSION['pseudo'] = $req['pseudo'];

				if($req['statut'] == "Inscrit")
				{
					header("Location: accueil_Adhérent.php");					
				}

				elseif($req['statut'] == 'administrateur')
				{
					header("location: admin/gestion.php");
				}

			}

		}
	}			
?>
	<img src="img/accueil.png" width="300" height="250">

	<a href="visiteur/accueil_visiteur.php">Je suis un visiteur</a>
	<form method="post">
	
	<?php
				
				if (isset($err_email)) {
				?>
					<script> alert(<?= $err_email ?>)</script>
				<?php
				}
			?>
		<input type="text" name="email" placeholder="adresse electronique" required> <br/>
		<?php
				
				if (isset($err_pass)) {
				?>
					<script> alert(<?= $err_pass ?>)</script>
				<?php
				}
			?>
		<input type="password" name="password" id="password" placeholder="mot de passe" required> <br/>
		<input type="submit" name="formsend" value="connection"> <br/>
		<a href="mdp_oublie.php">J'ai oublié mon mot de passe !!!!</a>
	</form>
	

</body>
</html>